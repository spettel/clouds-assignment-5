import { Injectable } from '@angular/core';
import {Router} from "@angular/router";

import { GoogleAuthProvider} from "@angular/fire/auth";
import {AngularFireAuth} from "@angular/fire/compat/auth";
import firebase from "firebase/compat";
import User = firebase.User;
import {Observable} from "rxjs";
import {WishlistService} from "./wishlist.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedIn = false;
  user? : User;
  authState? : Observable<firebase.User | null>

  constructor(private auth: AngularFireAuth, private router: Router) {
    this.authState = auth.authState;
    auth.authState.subscribe( (next) =>
    {
      if (next)
      {
        this.user = next;
        this.loggedIn = true;
        console.log("Logged in now");

      }
      else {
        this.user = undefined;
        this.loggedIn = false;
        console.log("Logged out now");
      }
    })

  }

  logOut() {
    this.auth.signOut().then( () =>
    {
      this.router.navigateByUrl("/");
    }).catch( error => {
      console.log("Logging out failed: ", error);
    })
  }

  googleLogin() {
    return this.auth.signInWithPopup(new GoogleAuthProvider());
  }
}

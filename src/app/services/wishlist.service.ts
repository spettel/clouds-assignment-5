import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList, AngularFireObject} from "@angular/fire/compat/database";
import {AuthService} from "./auth.service";
import {Observable} from "rxjs";
import {WishListDto} from "../model/wish-list-dto";

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  dto : WishListDto;
  item : AngularFireObject<WishListDto>;

  constructor(private db: AngularFireDatabase, private authService : AuthService) {
    this.dto = new WishListDto();
    this.item = this.db.object("unauth");

    authService.authState?.subscribe( (next) =>
    {
      if (next) {
        this.init();
      }
    })

  }

  addToWishList(id : number)
  {
    this.dto.addMovie(id);

    return this.item.set(this.dto);
  }

  removeFromWishList(id : number)
  {
    this.dto.removeMovie(id);

    if (this.dto.movieItems?.length == 0)
    {
      this.init();
      return this.item.remove();
    }
    else {
      return this.item.set(this.dto);
    }
  }

  getWishList(): Observable<WishListDto | null>
  {
    return this.item.valueChanges();
  }

  init()
  {
    console.log("WishlistService init");
    if (!this.authService.user)
    {
      throw new Error("User is not authenticated");
    }

    this.dto = new WishListDto();
    this.dto.uid = this.authService.user.uid;
    this.dto.movieItems = [];
    this.item = this.db.object(this.authService.user.uid);

    this.item.valueChanges().subscribe( res =>
    {
      if (res)
      {
        this.dto = new WishListDto();
        this.dto.uid = res.uid;
        this.dto.movieItems = res.movieItems;
        console.log("Loaded initial wishlist");
      }
    })
  }

}

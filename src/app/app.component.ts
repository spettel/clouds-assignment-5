import { Component } from '@angular/core';
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import { ReactiveFormsModule } from '@angular/forms';
import {AuthService} from "./services/auth.service";
import {AngularFireAuthGuard} from "@angular/fire/compat/auth-guard";
import {WishlistService} from "./services/wishlist.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private matIconReg : MatIconRegistry,
              private domSanitizer: DomSanitizer,
              private authService: AuthService) {

    this.matIconReg.addSvgIcon('movie-icon-new',
    this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/cinema-film.svg"))

    this.authService.authState?.subscribe( (next) =>
    {
      if (next)
      {
        if (next.displayName) {
          this.username = next.displayName
        }
      }
    })

  }

  title = 'ass5-clouds';
  username = "?";

  logOutClicked() {
    this.authService.logOut();
  }

  isAuthenticated() : boolean
  {
    return this.authService.loggedIn;
  }
}

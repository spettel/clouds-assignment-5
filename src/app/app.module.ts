import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { WishlistComponent } from './pages/wishlist/wishlist.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {HttpClientModule} from "@angular/common/http";
import {MatTableModule} from "@angular/material/table";
import {MatInputModule} from "@angular/material/input";
import {AngularFireModule} from "@angular/fire/compat";
import {AngularFireDatabaseModule} from "@angular/fire/compat/database";
import {firebaseConfig} from "../environments/environment";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {TableFilter} from "./pages/table.filter";


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    WishlistComponent,
    LoginPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    MatTableModule,
    MatInputModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    MatSnackBarModule,
  ],
  providers: [
    TableFilter
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

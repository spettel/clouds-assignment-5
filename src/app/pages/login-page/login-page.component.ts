import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {WishlistService} from "../../services/wishlist.service";
import {GoogleAuthProvider} from "@angular/fire/auth";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor(private auth : AuthService, private reg : MatIconRegistry,
              private domSan : DomSanitizer,
              private router: Router) {
    this.reg.addSvgIcon("google-icon", this.domSan.bypassSecurityTrustResourceUrl("../../assets/google.svg"))

  }

  ngOnInit(): void {
  }

  signIn() {
    this.auth.googleLogin().then(
      value => {
        console.log("Google authentication successful!", value);
        this.router.navigateByUrl("/dashboard");
      }).catch(error => {
      console.log("Error in Google authentication: ", error);
    });

  }
}

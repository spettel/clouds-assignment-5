import { Component, OnInit } from '@angular/core';
import {MovieDto} from "../../model/movie-dto";
import {MatTableDataSource} from "@angular/material/table";
import {AngularFireDatabase, AngularFireList} from "@angular/fire/compat/database";
import {map, Observable} from "rxjs";
import {Router} from "@angular/router";
import {WishlistService} from "../../services/wishlist.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {TableFilter} from "../table.filter";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  constructor(private db : AngularFireDatabase, private router: Router, private wishService: WishlistService,
              private snackBar: MatSnackBar, private tableFilter: TableFilter) {

  }

  movieList? : MatTableDataSource<MovieDto>;

  displayedColumns = ["title", "genre", "year", "add_to_wishlist"]

  lastFilter : String = "";

  assertMovieDto(elem: MovieDto): MovieDto {
    return elem;
  }

  ngOnInit(): void {

    let movies : AngularFireList<MovieDto> = this.db.list<MovieDto>("movies-list");

    movies.valueChanges().subscribe( val => {
        this.movieList = new MatTableDataSource<MovieDto>(val);
        this.tableFilter.setFilter(this.movieList);
        this.tableFilter.updateTableBasedOnWishList(this.movieList, this.wishService.dto);
      }
    );
  }

  applyFilter(event: KeyboardEvent) {
    if (this.movieList)
    {
      this.lastFilter = this.tableFilter.applyFilter(this.movieList, event)
    }
  }

  wishListClicked(movieDto: MovieDto) {

    if (movieDto.id)
    {
      this.wishService.addToWishList(movieDto.id).then( () =>
        {
          this.openSnackBar("Added to wishlist", "Dismiss")
          console.log("Added to wishlist");
          if (this.movieList) {
            this.tableFilter.updateTableBasedOnWishList(this.movieList, this.wishService.dto);
            this.movieList.filter = this.lastFilter.trim();
          }
        }).catch( err =>
      {
        console.log("Error: ", err);
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}

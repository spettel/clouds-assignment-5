import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MovieDto} from "../../model/movie-dto";
import {WishlistService} from "../../services/wishlist.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AngularFireDatabase} from "@angular/fire/compat/database";
import {TableFilter} from "../table.filter";
import {Router} from "@angular/router";

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {

  constructor(private wishService: WishlistService, private snackBar: MatSnackBar,
              private db: AngularFireDatabase, private tableFilter: TableFilter,
              private router: Router) { }

  movieList? : MatTableDataSource<MovieDto>;

  displayedColumns = ["title", "genre", "year", "remove_from_wishlist"]

  lastFilter : String = "";

  ngOnInit(): void {
    this.wishService.getWishList().subscribe((next) =>
    {
      if (next)
      {
        if (next.movieItems) {
          //firebase db does not have a WHERE in API, thus just download the whole list and filter then client-side
          let movies = this.db.list<MovieDto>("movies-list");
          let list_of_movies : MovieDto[] = [];

          movies.valueChanges().subscribe((movie_next) => {

            next.movieItems?.forEach((movie_id, idx) =>
            {
              movie_next.forEach( (val_movie, idx_movie) =>
              {
                if (val_movie.id == movie_id)
                {
                  list_of_movies.push(val_movie);
                }
              })
            })

            this.movieList = new MatTableDataSource<MovieDto>(list_of_movies);
            this.movieList.filter = this.lastFilter.trim();
            console.log("Loading list of movies is finished");
          })
        } else {
          this.movieList = new MatTableDataSource<MovieDto>();
        }
      }
      else {
        this.movieList = new MatTableDataSource<MovieDto>(); //empty list
      }
    })
  }


  assertMovieDto(elem: MovieDto): MovieDto {
    return elem;
  }

  wishListRemoveClicked(movieDto: MovieDto) {
    if (movieDto.id) {
      this.wishService.removeFromWishList(movieDto.id).then(() => {
        console.log("removed from wishlist")
        //update is done above as wishservice has a notification
      });
    }
  }

  applyFilter(event: KeyboardEvent) {
    if (this.movieList)
    {
      this.lastFilter = this.tableFilter.applyFilter(this.movieList, event)
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  toMovies() {
    this.router.navigateByUrl("/dashboard");
  }
}

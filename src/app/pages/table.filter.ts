import {Injectable} from "@angular/core";
import {MatTableDataSource} from "@angular/material/table";
import {MovieDto} from "../model/movie-dto";
import {WishListDto} from "../model/wish-list-dto";

@Injectable()
export class TableFilter {
  public setFilter(source: MatTableDataSource<MovieDto>) {

    source.filterPredicate = function (data, filter: string): boolean {
      filter = filter.toLowerCase();
      let year = "";
      let genre = "";
      let title = "";
      let id = 0;
      if (data.year) {
        year = data.year.toString();
      }
      if (data.genre) {
        genre = data.genre.toString().toLowerCase();
      }
      if (data.title) {
        title = data.title.toString().toLowerCase();
      }

      return (title.includes(filter) ||
        year.includes(filter) ||
        genre.includes(filter));
    }
  }

  updateTableBasedOnWishList(table: MatTableDataSource<MovieDto>, wishlist: WishListDto) {
    if (wishlist.movieItems) {
      //kinda inefficient code
      wishlist.movieItems.forEach((val1, idx1) => {
        table.data.forEach((val, idx) => {
          if (val1 == val.id) {
            table.data.splice(idx, 1);
          }
        })
      });

      table._updateChangeSubscription();
      console.log("Updated table based on wishlist");
    }
  }

  applyFilter(table: MatTableDataSource<MovieDto>, event: KeyboardEvent) : String
  {
    let target = event.target as HTMLInputElement;
    if (!target) return "";

    if (event.key == "Escape")
    {
      target.value = "";
    }

    console.log("Apply filter with value: " + target.value);
    let filter = target.value.trim().toLowerCase();
    table.filter = target.value.trim().toLowerCase();
    return filter;
  }

}

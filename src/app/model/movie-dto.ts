export class MovieDto {
  id?: number;
  genre?: string;
  title?: string;
  year?: number;
}

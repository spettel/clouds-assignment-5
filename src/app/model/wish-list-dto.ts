
export class WishListDto {
  uid?: string;
  movieItems?: number[];


  public addMovie(id: number)
  {
    let found = false;
    this.movieItems?.forEach( (value, idx) =>
    {
      if (value == id)
      {
        found = true;
      }
    })
    if (!found)
    {
      this.movieItems?.push(id);
    }
  }

  public removeMovie(id: number) {
    this.movieItems?.forEach((value, idx) => {
      if (value == id) {
        this.movieItems?.splice(idx, 1);
      }
    })
  }
}
